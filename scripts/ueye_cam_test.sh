#!/usr/bin/env bash

THIS_PATH="`dirname \"$0\"`"  
RECORD_FILE=/data/rosbag/ueye_cam.bag
DESTINATION_PATH=/data/rosbag/ueye_cam/
DEST_FILENAME=ueye_cam_test_

. /opt/ros/indigo/setup.bash
. $THIS_PATH/../catkin_ws/devel/setup.bash

roslaunch common px4.launch &
rosrun image_view image_view image:=/cam0/image_raw&
roslaunch common rosbag_record_video.launch&
roslaunch common trigger.launch &
sleep 1s
roslaunch common camera_synch.launch

# wait for stabilisation
sleep 2s
#roslaunch common rosbag_record_video.launch


DATE_STRING=`date +%Y.%m.%d.%H.%m.%S`
echo "Date string: $DATE_STRING"
echo "save rosbag to $DESTINATION_PATH$DEST_FILENAME$DATE_STRING.bag"
mv $RECORD_FILE $DESTINATION_PATH$DEST_FILENAME$DATE_STRING.bag

