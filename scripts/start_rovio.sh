#!/usr/bin/env bash

THIS_PATH="`dirname \"$0\"`"  
ROS_BAG_DIR=/data/rosbag

RECORD_FILE=$ROS_BAG_DIR/recorded.bag
DESTINATION_PATH=$ROS_BAG_DIR/kalibr/
DEST_FILENAME=camera_kalibr_dynamic_
mkdir -p $DESTINATION_PATH


. /opt/ros/indigo/setup.bash
. $THIS_PATH/../catkin_ws/devel/setup.bash

roslaunch common rovio_node.launch


