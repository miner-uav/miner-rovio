#!/usr/bin/env bash

set -e 

THIS_PATH="`dirname \"$0\"`"  
SOURCE_FILE=/data/rosbag/recorded.bag
DEST_PATH=/media/odroid/SAMSUNG_SD/Experiments
EXPERIMENT_ID=$1
EXPERIMENT_PATH=$DEST_PATH/$EXPERIMENT_ID

if [ ! -f $SOURCE_FILE ]; then
    echo "ERROR: Source file does not exists"
    exit -1
fi

if [ ! -d $DEST_PATH ]; then
    echo "ERROR: destination path does not exists: $DEST_PATH"  
    exit -1
fi

if [[ -z "${EXPERIMENT_ID// }" ]]; then
    echo "ERROR: Experiment id can not be empty..."  
    exit -1
fi

if [ -d $EXPERIMENT_PATH ]; then
    echo "ERROR: Path for this experiment already used: $EXPERIMENT_PATH"  
    exit -1
fi

mkdir $DEST_PATH/$EXPERIMENT_ID

DATE_STRING=`date +%Y.%m.%d_%H.%M.%S`

rsync -h --progress $SOURCE_FILE $EXPERIMENT_PATH/${EXPERIMENT_ID}_${DATE_STRING}.bag
ls -lah $EXPERIMENT_PATH/${EXPERIMENT_ID}_${DATE_STRING}.bag
rm $SOURCE_FILE

sudo umount /media/odroid/SAMSUNG_SD
echo "SD CARD Unmounted - you can remove it."

