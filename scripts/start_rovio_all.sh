#!/usr/bin/env bash

THIS_PATH="`dirname \"$0\"`"  
ROS_BAG_DIR=/data/rosbag

RECORD_FILE=$ROS_BAG_DIR/recorded.bag
DESTINATION_PATH=$ROS_BAG_DIR/rovio
mkdir -p $DESTINATION_PATH

DATE_STRING=`date +%Y.%m.%d_%H.%M.%S`


. /opt/ros/indigo/setup.bash
. $THIS_PATH/../catkin_ws/devel/setup.bash

ARGS="bag_file_name:=${DESTINATION_PATH}/rovio_${DATE_STRING}.bag"

roslaunch common bundle_rovio.launch $ARGS


