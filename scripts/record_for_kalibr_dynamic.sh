#!/usr/bin/env bash

THIS_PATH="`dirname \"$0\"`"  
ROS_BAG_DIR=/data/rosbag

RECORD_FILE=$ROS_BAG_DIR/recorded.bag
DESTINATION_PATH=$ROS_BAG_DIR/kalibr/
DEST_FILENAME=camera_kalibr_dynamic_
mkdir -p $DESTINATION_PATH


. /opt/ros/indigo/setup.bash
. $THIS_PATH/../catkin_ws/devel/setup.bash

roslaunch common rosbag_record.launch&
roslaunch common px4.launch &
sleep 5s
roslaunch common camera_synch.launch cam_frame_rate:=20.0  cam_exposure:=3 cam_auto_exposure:=False

# wait for stabilisation
sleep 2s

DATE_STRING=`date +%Y.%m.%d.%H.%M.%S`
echo "save rosbag to $DESTINATION_PATH$DEST_FILENAME$DATE_STRING.bag"

rsync -h --progress $RECORD_FILE $DESTINATION_PATH$DEST_FILENAME$DATE_STRING.bag
ls -lah $DESTINATION_PATH$DEST_FILENAME$DATE_STRING.bag
rm $RECORD_FILE
