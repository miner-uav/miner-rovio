#!/usr/bin/env bash

THIS_PATH="`dirname \"$0\"`"  


. /opt/ros/indigo/setup.bash
. $THIS_PATH/../catkin_ws/devel/setup.bash

roslaunch common camera_synch.launch cam_frame_rate:=20 cam_exposure:=3 cam_time_synch_method:=0


