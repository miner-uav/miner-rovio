#!/usr/bin/env bash

THIS_PATH="`dirname \"$0\"`"  


. /opt/ros/indigo/setup.bash
. $THIS_PATH/../catkin_ws/devel/setup.bash

roslaunch common bundle_mavros_and_camera.launch


